
package py.una.entidad;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class datosmetereologicos {

    private Integer id_estacion;
    private String ciudad;
    private Float porcentaje_humedad;
    private Integer temperatura;
    private Float velocidad_viento;
    private String fecha;

    public datosmetereologicos(Integer Did_estacion, String Dciudad, Float Dporcentaje_humedad, Integer Dtemperatura, Float Dvelocidad_viento, String Dfecha) {
        this.id_estacion = Did_estacion;
        this.ciudad = Dciudad;
        this.porcentaje_humedad = Dporcentaje_humedad;
        this.temperatura = Dtemperatura;
        this.velocidad_viento = Dvelocidad_viento;
        this.fecha = Dfecha;
        //this.hora= Dhora;

    }

    public void setId_estacion(Integer id_estacion) {
        this.id_estacion = id_estacion;
    }

    public datosmetereologicos()
    {}
    
    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Float getPorcentaje_humedad() {
        return porcentaje_humedad;
    }

    public void setPorcentaje_humedad(Float porcentaje_humedad) {
        this.porcentaje_humedad = porcentaje_humedad;
    }

    public Integer getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Integer temperatura) {
        this.temperatura = temperatura;
    }

    public Float getVelocidad_viento() {
        return velocidad_viento;
    }

    public void setVelocidad_viento(Float velocidad_viento) {
        this.velocidad_viento = velocidad_viento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getId_estacion() {
        return id_estacion;
    }

}
