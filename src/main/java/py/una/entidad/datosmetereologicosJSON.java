
package py.una.entidad;

import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class datosmetereologicosJSON {
    
    public static void main(String[] args) throws Exception {
    	datosmetereologicosJSON representacion = new datosmetereologicosJSON();
    	
    	System.out.println("Ejemplo de uso 1: pasar de objeto a string");
    	datosmetereologicos p = new datosmetereologicos();
    	p.getId_estacion();
        
    	p.getCiudad();
        p.setCiudad("San Lorenzo");
        p.getPorcentaje_humedad();
        p.setPorcentaje_humedad(1.56f);
        
        p.getTemperatura();
        p.setTemperatura(23);
        
        p.getVelocidad_viento();
        p.setVelocidad_viento(2.78f);
        
    	p.getFecha();
        p.setFecha("24/08/22");
        
    	
    	String r1 = representacion.objetoString(p);
    	System.out.println(r1);
    	
    	
    	
    	String un_string = "{\"ciudad\":San Lorenzo,\"humedad\":\"15\",\"velocidad\":\"40\",\"fecha\":[\"180922\"]}";
    	
    	datosmetereologicos r2 = representacion.stringObjeto(un_string);
    	System.out.println(r2.getId_estacion() + " " + r2.getCiudad() + " " +r2.getPorcentaje_humedad() + " " +r2.getTemperatura() + " " +r2.getVelocidad_viento()+ " " + " " +r2.getFecha() );
        
    }
    
    public static String objetoString(datosmetereologicos p) {	
    	
		JSONObject obj = new JSONObject();
        obj.put("id_estacion", p.getId_estacion());
        obj.put("ciudad", p.getCiudad());
        obj.put("porcentaje_humedad", p.getPorcentaje_humedad());
        obj.put("temperatura", p.getTemperatura());
        obj.put("velocidad_viento", p.getVelocidad_viento());
        obj.put("fecha", p.getFecha());

        JSONArray list = new JSONArray();
        
        
       // if(list.size() > 0) {
        	obj.put("asignaturas", list);
        //}
        

        return obj.toJSONString();
    }
    
    
    public static datosmetereologicos stringObjeto(String str) throws Exception {
    	datosmetereologicos p = new datosmetereologicos();
        JSONParser parser = new JSONParser();

        Object obj = parser.parse(str.trim());
        JSONObject jsonObject = (JSONObject) obj;

       
         p.setId_estacion((int)jsonObject.get("estacion"));
        p.setCiudad(jsonObject.get("ciudad").toString());
        p.setPorcentaje_humedad((float)jsonObject.get("humedad"));
        
         p.setTemperatura((int)jsonObject.get("temperatura"));
          p.setVelocidad_viento((float)jsonObject.get("velocidad"));
         p.setFecha(jsonObject.get("fecha").toString());
          
        JSONArray msg = (JSONArray) jsonObject.get("asignaturas");
        Iterator<String> iterator = msg.iterator();
        
        return p;
	}   
}
