package py.una.server.tcp;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import java.util.Scanner;


public class TCPCliente {

    public static void main(String[] args) throws IOException {

        Socket unSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;
        Scanner lectura = new Scanner(System.in);
        

        try {
            unSocket = new Socket("localhost", 4444);
            
            out = new PrintWriter(unSocket.getOutputStream(), true);

          
            in = new BufferedReader(new InputStreamReader(unSocket.getInputStream()));
        } catch (UnknownHostException e) {
            
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Error de I/O en la conexion al host");
            System.exit(1);
        }

        
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        MensajeCliente mensaje = new MensajeCliente();
        
        String fromServidor;
        String fromUsuario = null;

        while ((fromServidor = in.readLine()) != null) {
            
            System.out.println("Opcion: ");
            Integer opcion = lectura.nextInt();
            lectura.nextLine();
            switch(opcion) {
                case 1:
                        System.out.println("Nombre : Matias Alexis Miranda Sosa  ");
                        System.out.println("Edad: 22  ");
                        
                        System.out.println("Ingresar datos: ");
                        System.out.println("ID Estacion: ");
                        Integer id_estacion = Integer.parseInt(lectura.nextLine());
                        System.out.println("Ciudad: ");
                        String ciudad = lectura.nextLine();
                        System.out.println("Temperatura: ");
                        Integer temperatura = lectura.nextInt();
                        System.out.println("Velocidad del Viento: ");
                        Float velocidad_viento = lectura.nextFloat();
                        lectura.nextLine();
                        System.out.println("Fecha (AAAAMMDD): ");
                        String fecha = lectura.nextLine();
                       
                        out.println(fromUsuario);
                        fromUsuario="";
                    break;
                case 2:
                    System.out.println("Nombre : Matias Alexis Miranda Sosa  ");
                        System.out.println("Edad: 22  ");
                    System.out.println("Consultar temperatura: ");
                    String temperaturaConsulta = lectura.nextLine();
                   fromUsuario = mensaje.createJSONString(opcion, temperaturaConsulta);
                    
                    fromUsuario="";
                    
                    fromServidor = in.readLine();
                    if (fromServidor == null) {
                        System.out.println("No se recibio el mensaje");
                        System.out.println("Fin de la ejecucion");
                        System.exit(0);
                    }
                    default:
                    System.out.println("Operacion No Soportada");
            }
           
        }

        out.close();
        in.close();
        stdIn.close();
        unSocket.close();
    }
}
