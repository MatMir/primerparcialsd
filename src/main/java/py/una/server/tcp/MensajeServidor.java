
package py.una.server.tcp;

import org.json.simple.*;
import org.json.simple.parser.*;

public class MensajeServidor {
    
    public JSONObject mensajeRecibido(String mensaje) {
        JSONParser parser = new JSONParser();
        Object aux = null;
        try {
            aux = parser.parse(mensaje.trim());
        } catch (ParseException e) {
            System.out.println("Error");
        }
        JSONObject auxiliar = (JSONObject) aux;
        return auxiliar;
    }

}

