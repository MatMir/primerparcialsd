
package py.una.server.tcp;

    import org.json.simple.*;
import org.json.simple.parser.*;
import py.una.entidad.datosmetereologicos;

public class MensajeCliente {
    
    public String createJSONString(String id_cliente,datosmetereologicos d, Integer opcion) {
        JSONObject auxiliar =  new JSONObject();
        
        auxiliar.put("id_cliente", id_cliente);
        auxiliar.put("opcion", opcion);
        auxiliar.put("id_estacion", d.getId_estacion());
        auxiliar.put("ciudad", d.getCiudad());
        auxiliar.put("porcentaje_humedad", d.getPorcentaje_humedad());
        auxiliar.put("temperatura", d.getTemperatura());
        auxiliar.put("velocidad_viento", d.getVelocidad_viento());
        auxiliar.put("fecha", d.getFecha());
        
        
        return auxiliar.toJSONString();
    }
    
    public String createJSONString(String id_cliente, Integer opcion, String mensaje) {
        JSONObject auxiliar = new JSONObject();
        auxiliar.put("id_cliente", id_cliente);
        auxiliar.put("opcion", opcion);
        auxiliar.put("contenido", mensaje);
        
        return auxiliar.toJSONString();
    } 
    
    public JSONObject mensajeRecibido(String mensaje) {
        JSONParser parser = new JSONParser();
        Object aux = null;
        try {
            aux = parser.parse(mensaje.trim());
        } catch (ParseException e) {
            System.out.println("Error");
        }
        JSONObject auxiliar = (JSONObject) aux;
        return auxiliar;
    }
      
}
