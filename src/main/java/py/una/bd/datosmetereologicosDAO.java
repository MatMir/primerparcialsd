
package py.una.bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import py.una.entidad.datosmetereologicos;

public class datosmetereologicosDAO {
 
 public List<datosmetereologicos> seleccionar() {
		String query = "SELECT id_estacion, ciudad, porcentaje_humedad,temperatura,velocidad_viento,fecha FROM datos_metereologicos ";
		
		List<datosmetereologicos> lista = new ArrayList<datosmetereologicos>();
		
		Connection conn = null; 
        try 
        {
        	conn = BD.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);

        	while(rs.next()) {
        		datosmetereologicos p = new datosmetereologicos();
        		
                        p.setCiudad(rs.getString(1));
        		p.setPorcentaje_humedad(rs.getFloat(2));
        		p.setTemperatura(rs.getInt(3));
                        
                        p.setVelocidad_viento(rs.getFloat(4));
                        p.setFecha(rs.getString(5));
                        
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            System.out.println("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	public List<datosmetereologicos> seleccionarPorCedula(long cedula) {
		String SQL = "SELECT cedula, nombre, apellido FROM persona WHERE cedula = ? ";
		
		List<datosmetereologicos> lista = new ArrayList<datosmetereologicos>();
		
		Connection conn = null; 
        try 
        {
        	conn = BD.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, cedula);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		datosmetereologicos p = new datosmetereologicos();
        		
                        p.setCiudad(rs.getString(1));
        		p.setPorcentaje_humedad(rs.getFloat(2));
        		p.setTemperatura(rs.getInt(3));
                        
                        p.setVelocidad_viento(rs.getFloat(4));
                        p.setFecha(rs.getString(5));
        		
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            System.out.println("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
    public long insertar(datosmetereologicos p) throws SQLException {

        String SQL = "INSERT INTO persona(cedula, nombre,apellido) "
                + "VALUES(?,?,?)";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = BD.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
           
                        pstmt.setString(1, p.getCiudad());
                        pstmt.setFloat(2, p.getPorcentaje_humedad());
                        
                        pstmt.setInt(3, p.getTemperatura());
                        pstmt.setFloat(4, p.getVelocidad_viento());
                        
                        pstmt.setString(5, p.getFecha());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la insercion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return id;
    	
    	
    }
	

    public long actualizar(datosmetereologicos p) throws SQLException {

        String SQL = "UPDATE persona SET nombre = ? , apellido = ? WHERE cedula = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = BD.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            
                        pstmt.setString(1, p.getCiudad());
                        pstmt.setFloat(2, p.getPorcentaje_humedad());
                        
                        pstmt.setInt(3, p.getTemperatura());
                        pstmt.setFloat(4, p.getVelocidad_viento());
                        
                        pstmt.setString(5, p.getFecha());
                
                
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
    
    public long borrar(long cedula) throws SQLException {

        String SQL = "DELETE FROM persona WHERE cedula = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = BD.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, cedula);
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la eliminación: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }   
    
}
