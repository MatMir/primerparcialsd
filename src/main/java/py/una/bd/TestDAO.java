package py.una.bd;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import py.una.entidad.datosmetereologicos;

public class TestDAO {
    
    public static void main(String args[]) throws SQLException{
		
		datosmetereologicosDAO aux = new datosmetereologicosDAO();
		
		
	      aux.insertar(new datosmetereologicos(24,"San Lorenzo",1.56f,23,2.78f,"24/08/22") );
	      aux.insertar(new datosmetereologicos(25,"Ypane",2.56f,5,3.59f,"12/06/22") );
              aux.insertar(new datosmetereologicos(26,"Fernando de la Mora",3.56f,67,1.26f,"6/02/22") );
              aux.insertar(new datosmetereologicos(27,"Capiata",4.56f,23,9.45f,"19/11/22") );
                
		
		
		aux.actualizar(new datosmetereologicos(14,"Carapegua",2.91f,15,2.58f,"29/12/22") );
		
		aux.borrar(203L);
		
		
		
		List<datosmetereologicos> lista = aux.seleccionar(); 
		
		
		for (datosmetereologicos p: lista){
			System.out.println(p.getCiudad() + " " + p.getPorcentaje_humedad() + " " + p.getTemperatura()+ " "+ p.getVelocidad_viento() + " "+ p.getFecha()+ " "+ p.getId_estacion() );
		}
	}
    
}
